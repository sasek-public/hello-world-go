############################
# STEP 1 build executable binary
############################
FROM golang:1.15-alpine

WORKDIR /go/src/hello-server/
COPY hello-server.go .
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o app .

############################
# STEP 2 build container with compiled image
############################
FROM alpine:latest
RUN apk --no-cache add ca-certificates
WORKDIR /root/
COPY --from=0 /go/src/hello-server/app .
CMD ["./app"]
