package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
)

type HelloServer struct {
	http.Handler
}

func main() {
	server := NewHelloServer()
	if err := http.ListenAndServe(":5000", server); err != nil {
		log.Fatalf("could not listen on port 5000 %v", err)
	}
}
func NewHelloServer() *HelloServer {
	s := new(HelloServer)
	router := http.NewServeMux()

	router.Handle("/hello", http.HandlerFunc(s.helloHandler))
	router.Handle("/greg", http.HandlerFunc(s.gregHandler))
	s.Handler = router
	return s
}

func (server *HelloServer) helloHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Hello world")
}

func (server *HelloServer) gregHandler(w http.ResponseWriter, r *http.Request) {
	json.NewEncoder(w).Encode(Greg)
}

type User struct {
	Name  string `json:"Name"`
	Email string `json:"Email"`
}

var Greg = User{Email: "greg@mailinator.com", Name: "Greg"}
