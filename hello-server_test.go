package main

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestGetGreg(t *testing.T) {
	server := NewHelloServer()

	t.Run("returns Greg's Json", func(t *testing.T) {
		// given
		request, _ := http.NewRequest(http.MethodGet, "/greg", nil)
		response := httptest.NewRecorder()

		want := User{Email: "greg@mailinator.com", Name: "Greg"}
		var got User

		// when
		server.ServeHTTP(response, request)
		err := json.NewDecoder(response.Body).Decode(&got)

		// then
		if response.Code != http.StatusOK {
			t.Errorf("Invalid status: %q", response.Code)
		}

		if err != nil {
			t.Errorf("Decoding error: %q", err)
		}

		if want != got {
			t.Errorf("got %q, want %q", got, want)
		}
	})
}
