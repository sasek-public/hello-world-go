## Golang hello world
This is a minimal web server for POC usage.

The app is basically copied from:
https://quii.gitbook.io/learn-go-with-tests/build-an-application/json

***

### Running locally without Docker
##### 1. Run the server

`> go run hello-server.go`
##### 2. Get the data

`> curl http://localhost:5000/greg`

### Running witn Docker
##### 1. Build the image
`> docker build -t hello-world-go .`

##### 2. Run the container
`> docker run -p 5000:5000 -it hello-world-go`

##### 3. Get the data
`> curl http://localhost:5000/greg`